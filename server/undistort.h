
#ifndef UNDISTORT_H
#define UNDISTORT_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>


// undistort_map_t points to a big array of these, 1 per pixel
typedef struct bilinear_lookup_t{
	int16_t		I[2]; // backwards map to top left corner of 2x2 square
	uint8_t		F[4]; // 4 coefficients for 4 corners of square
} bilinear_lookup_t;


typedef struct undistort_map_t{
	// config values set by you
	int w;			// image width
	int h;			// image height
	float fx;		// focal length in pixels
	float fy;		// focal length in pixels
	float cx;		// optical center in pixels
	float cy;		// optical center in pixels
	int n_coeffs;	// 4 for fisheye!
	float D[12];	// distortion coeffs
	int fisheye;	// for now, assumed to be true
	float scale;	// zoom in or out at the same time. 1.0 to leave focal length the same

	// dynamic values set by init_undistort_map()
	float fxrect;	// rectified focal length
	float fyrect;	// rectified focal length
	bilinear_lookup_t* L; // lookup table
} undistort_map_t;



int mcv_init_undistort_map(undistort_map_t* map);

int mcv_undistort_image(const uint8_t* input, uint8_t* output, undistort_map_t* map);




#ifdef __cplusplus
}
#endif

#endif // UNDISTORT_H

