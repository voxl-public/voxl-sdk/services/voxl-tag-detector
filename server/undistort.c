
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "undistort.h"


// AVG 1.4ms min 1.20 ms for vga image on VOXL1 fastest core
int mcv_undistort_image(const uint8_t* input, uint8_t* output, undistort_map_t* map)
{
	// shortcut variables to make code cleaner
	int  height = map->h;
	int   width = map->w;
	int n_pix = width*height;
	bilinear_lookup_t* L = map->L;

	// go through every pixel in input image
	for(int pix=0; pix<n_pix; pix++){

		// check for invalid (blank) pixels
		if(L[pix].I[0]<0){
			output[pix] = 0;
			continue;
		}

		// get indices from index lookup I
		uint16_t x1 = L[pix].I[0];
		uint16_t y1 = L[pix].I[1];

		// don't worry about all the index algebra, the compiler optimizes this
		uint16_t p0 = input[width*y1 + x1];
		uint16_t p1 = input[width*y1 + x1 + 1];
		uint16_t p2 = input[width*(y1+1) + x1];
		uint16_t p3 = input[width*(y1+1) + x1 + 1];

		// multiply add each pixel with weighting
		output[pix] = (	p0*L[pix].F[0] +
						p1*L[pix].F[1] +
						p2*L[pix].F[2] +
						p3*L[pix].F[3])/256;

	}

	return 0;
}



int mcv_init_undistort_map(undistort_map_t* map)
{
	// Preparation
	float fx = map->fx; // original focal length
	float fy = map->fy; // original focal length
	float cx = map->cx; // original center
	float cy = map->cy; // original center
	float fx_new = map->scale*fx; // new focal length
	float fy_new = map->scale*fy; // new focal length
	float cx_new = cx; // new center, keep in case we do resolution scaling in the future
	float cy_new = cy; // new center, keep in case we do resolution scaling in the future
	int height = map->h;
	int width = map->w;
	float* D = map->D;

	// save new scaled focal length for user to access
	map->fxrect = fx_new;
	map->fyrect = fy_new;

	// allocate new map
	// TODO some sanity and error checking here
	map->L = (bilinear_lookup_t*)malloc(height*width*sizeof(bilinear_lookup_t));
	if(map->L==NULL){
		perror("failed to allocate memory for lookup table");
		return -1;
	}
	bilinear_lookup_t* L = map->L;

	// distorted world coordinate
	float xwd,ywd;

	for(int v=0; v<height; ++v){
		for(int u=0; u<width; ++u){

			// Find the undistorted world coordinate for each new pixel in
			// the rectified image we wish to create
			float xwu = (u - cx_new) / fx_new;
			float ywu = (v - cy_new) / fy_new;


			// opencv fisheye model with 4 distortion parameters
			if(map->fisheye && map->n_coeffs==4){
				float r = sqrt(xwu*xwu + ywu*ywu);
				float theta = atan2(r, 1);

				// Perform distortion (forward map)
				float theta_d = theta*(1 + D[0]*powf(theta,2) + D[1]*powf(theta,4) +
						D[2]*powf(theta,6) + D[3]*powf(theta,8));

				// now find the world-frame location when distorted
				xwd = (theta_d/r)*xwu;
				ywd = (theta_d/r)*ywu;
			}

			// 5 coefficient model k1k2p1p2k3 from opencv
			else if(!map->fisheye && map->n_coeffs==5){

				float k1 = map->D[0];
				float k2 = map->D[1];
				float p1 = map->D[2];
				float p2 = map->D[3];
				float k3 = map->D[4];

				float r2 = xwu*xwu + ywu*ywu;
				float r4 = r2*r2;
				float r6 = r4*r2;

				// now find the world-frame location when distorted
				xwd = (xwu * (1.0f + k1*r2 + k2*r4 + k3*r6)) +
							(2.0f*p1*xwu*ywu) + (p2*(r2 + 2.0f*xwu*xwu));

				ywd = (ywu * (1.0f + k1*r2 + k2*r4 + k3*r6)) +
							(p1*(r2 + 2.0f*ywu*ywu)) + (2.0f*p2*xwu*ywu);
			}
			else{
				fprintf(stderr, "ERROR in %s, only support 4-coefficient fisheye and 5-coefficient polynomial models\n", __FUNCTION__);
				return -1;
			}

			// convert the distorted world coordinate to pixel coordinate
			// in the original distorted image
			float xd = (fx * xwd) + cx;
			float yd = (fy * ywd) + cy;

			// find 4 corners around that subpixel
			int x1 = floor(xd);
			int x2 = x1+1;
			int y1 = floor(yd);
			int y2 = y1+1;

			// flag a pixel invalid
			int pix = width*v + u;
			if((x1 < 0 || x2 > (width-1)) || (y1 < 0 || y2 > (height-1))){
				L[pix].I[0] = -1;
				L[pix].I[1] = -1;
				continue;
			}

			// populate lookup table with top left corner pixel
			L[pix].I[0] = x1;
			L[pix].I[1] = y1;

			// integer weightings for 4 pixels. Due to truncation, these 4 ints
			// should sum to no more than 255
			L[pix].F[0] = (y2-yd)*(x2-xd)*256;
			L[pix].F[1] = (y2-yd)*(xd-x1)*256;
			L[pix].F[2] = (yd-y1)*(x2-xd)*256;
			L[pix].F[3] = (yd-y1)*(xd-x1)*256;
		}
	}


	return 0;
}


